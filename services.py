import json
import requests
import re
from datetime import datetime
import telebot
import time
from config import BOT_TOKEN, CHAT_ID


def get_urls():
    """Получаем список ссылок из json файла"""
    with open('urls.json', 'r', encoding='UTF-8') as file:
        data = json.load(file)
        urls = [i['url'] for i in data]
        return urls


def get_proxies_errors_list(url):
    """Получаем список зафиксированных ошибок по ссылке"""
    try:
        response = requests.get(url=url)
        if response.status_code == 200:
            data = response.json().get('proxies')
            return data
    except requests.exceptions.ConnectionError:
        pass


def create_message_text(proxy_error_data: dict):
    """Составляем сообщение для отправки в телеграм-чат из словаря"""
    date_time = proxy_error_data.get('date_time')
    date_time = datetime.strptime(date_time, '%Y.%m.%d, %H:%M:%S').strftime('%d.%m.%Y %H:%M:%S')
    proxy_error_data['date_time'] = date_time
    text = [f"<b>{key}</b>: {proxy_error_data[key]}" for key in proxy_error_data]
    title = '<b>Информация об ошибке:\n</b>'
    text.insert(0, title)
    text = '\n'.join(text)
    text = re.sub('server', 'Сервер', text)
    text = re.sub('city', 'Город', text)
    text = re.sub('operator', 'Оператор', text)
    text = re.sub('date_time', 'Дата и время инцидента', text)
    return text


def send_message(text: str):
    """Отправляем сообщение об ошибке в чат"""
    bot = telebot.TeleBot(token=BOT_TOKEN, parse_mode='HTML')
    while True:
        try:
            bot.send_message(chat_id=-CHAT_ID, text=text)
            break
        except telebot.apihelper.ApiTelegramException:
            time.sleep(30)
            pass







