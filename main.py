from services import get_urls, get_proxies_errors_list, create_message_text, send_message
from db.models import UpTimeErrorsWorker

up_time_errors_worker = UpTimeErrorsWorker()


def main():
    urls = get_urls()
    for url in urls:
        proxies_errors_list = get_proxies_errors_list(url=url)
        for proxy_error_data in proxies_errors_list:
            if up_time_errors_worker.is_record_exist(proxy_error_data=proxy_error_data):
                break
            up_time_errors_worker.log_uptime_error_record(proxy_error_data=proxy_error_data)
            text = create_message_text(proxy_error_data=proxy_error_data)
            send_message(text=text)


if __name__ == '__main__':
    main()



